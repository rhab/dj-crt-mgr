from django.apps import AppConfig


class DjCrtMgrConfig(AppConfig):
    name = 'dj_crt_mgr'
