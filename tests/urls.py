from django.http import Http404, HttpResponse
from django.urls import path


def test_view(request):
    if request.method != "GET":
        raise Http404()
    return HttpResponse("Test view")


urlpatterns = [
    path("", test_view),
]
